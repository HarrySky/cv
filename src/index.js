import React from 'react';
import ReactDOM from 'react-dom';
import HomeContainer from './containers';
import './index.css';

ReactDOM.render(<HomeContainer />, document.getElementById('root'));
